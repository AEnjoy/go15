# Web全栈开发

Vblog项目介绍与骨架搭建:
+ 项目概要设计: 需求,流程,原型与架构
+ 项目详细设计: API接口设计,数据库表结构设计
+ 业务代码组织流程设计(面向对象的程序设计)
+ 项目架构风格: 功能分区与业务分区架构(面向业务DDD简化架构)

详细说明:
+ 项目课整体介绍
+ 软件开发生命周期流程介绍
+ 项目需求概述: Markdown博客管理系统
+ 项目原型草图: 访客前提与博客管理后台
+ 项目技术栈与架构: Gin+GROM+Vue3+ArcoDesign
+ 项目RESTful风格API设计
+ 项目中的单元测试与Vscode单元测试配置
+ Vscode中如何对Go项目进行单步调试
+ 如何编写功能分区架构风格的代码(MVC)
+ 如果编写业务分区架构风格的代码(DDD)

## 项目需求概述: 博客发布，读者阅读

Markdown文档管理需求:
+ 笔记管理: Markdonw笔记
+ GitBook: 书籍组织
+ 内部知识库: 某个专题的文章收录

我们只做Mardonw博客网站:
+ 博客作者,内容生产
+ 博客读者, 阅读内容

## 项目原型草图: 访客前台与博客管理后台

参考的产品: [我的博客](https://yumaojun03.github.io/)

1. 访客前台页面: 
+ ![](https://gitee.com/infraboard/go-course/raw/master/image/projects/frontend-blog-list.png)
+ 内容详情 直接通过UI展示 Markdown内容

2. 博客管理后台
+ ![登录页](https://gitee.com/infraboard/go-course/raw/master/image/projects/vblog_login.png)
+ ![文章列表页](https://gitee.com/infraboard/go-course/raw/master/image/projects/backend-blog-list.png)
+ ![文章编辑创建页](https://gitee.com/infraboard/go-course/raw/master/image/projects/blog-edit.png)


## 项目技术栈与架构

![项目架构](image.png)

前端: 
+ [Vue3(HTML/CSS/JS)](http://vue.org/)
+ [ArcoDesign](https://arco.design/vue/component/auto-complete)

后端:
+ Gin
+ GROM

## RESTful接口设计

API: 程序编程接口
+ 资源(用户的数据), 作者编写的博客, 业务功能对应的数据
+ 资源动作: 创建文章,编写文章,....

接口风格:
+ SOAP:  HTTP(POST)/XML,   post /url/password/change  body
+ RESTFUL: HTTP(HTTP method)/JSON

什么是RESTful风格API? 需要合理的对资源进行设计(拆解成子资源)

HTTP method:
+ get
+ post
+ put
+ patch
+ delete
+ options
+ ...

REST: (resource) representational state transfer:  (资源)状态转化, 用户的操作的资源的状态进行了变更, URL命名: 以资源进行结尾 xxxxx/users, xxxx/blogs, xxxx/books
+ get: 获取一类资源,  xxx/books, filter: url query string
+ get: 获取单个资源, xxxx/books/{id}
+ post: 创建资源, xxxx/books,  http body
+ put/patch: 修改一个资源, put: 全量修改, patch 增量修改, xxxx/books/{id}, http body
+ delete: 删除一个资源，xxxx/books/{id}


抽象出服务端的资源和操作

